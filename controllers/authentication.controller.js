"use strict";
const ApiResponse = require('../utils/apiResponse');
const UserModel = require('../models/user');
const Session = require('../models/session');
const bcrypt = require('bcrypt');
const randtoken = require('rand-token').suid;
const errorsHandler = require('../dataBase/dbErrorsHandler');
const constents = require('../utils/constants');

class Authentication {
    async login(userData) {
        let errors = [];
        if (!userData.email) {
            errors.push({
                "email": 'user email is requierd'
            });
        }
        if (!userData.password) {
            errors.push({
                "password": 'user password is requierd'
            });
        }
        if (errors.length > 0) {
            return ApiResponse(errors, null);
        } else {
            return UserModel.findOne({
                email: userData.email
            }).then(async (user) => {
                if (user) {
                    //check user password if match create asession
                    const match = bcrypt.compareSync(userData.password, user.password);
                    if (match) {
                        await Session.deleteOne({
                            userId: user._id
                        });
                        let newSession = new Session({
                            userId: user._id,
                            createdAt: new Date().getTime(),
                            token: randtoken(16)
                        });
                        return newSession.save().then((session) => {
                            return ApiResponse(null, [{
                                token: session.token
                            }]);
                        }).catch((err) => {
                            return ApiResponse([{
                                msg: 'authentication failed'
                            }], null);
                        });

                    } else {
                        errors.push({
                            msg: 'password is incorrect'
                        });
                        return ApiResponse(errors, null)
                    }

                } else {
                    errors.push({
                        msg: 'email is incorrect'
                    });
                    return ApiResponse(errors, null)
                }

            }).catch((err) => {
                return ApiResponse([{
                    msg: 'faild to find user'
                }], null);
            });
        }
    }

    async create(user) {
        //check user data and register
        let hashedPassword = user.password;
        // check password for null and lenght 8
        if (hashedPassword && hashedPassword.length >= constents.Password.minLenght) {
            hashedPassword = bcrypt.hashSync(user.password, constents.Password.bcryptFactor);
        }
        //convert date
        if (user.weddingDate) {
            user.weddingDate = new Date(`${user.weddingDate}`).getTime();
        }
        let newUser = new UserModel({
            name: user.name,
            email: user.email,
            password: hashedPassword,
            mobile: user.mobile,
            weddingDate: user.weddingDate,
            role: user.role,
            cityId: user.cityId,
            countryId: user.countryId
        });
        if (newUser.role != null && newUser.role === 'trader') {
            newUser.weddingDate = '0';
        }
        return await save(newUser);
    }
}

function save(newUser) {
    return newUser.save().then((data) => {
        return ApiResponse(null, {
            msg: 'User created successfully'
        });
    }).catch((err) => {
        return ApiResponse(errorsHandler(err), null);
    });
}

module.exports = new Authentication();