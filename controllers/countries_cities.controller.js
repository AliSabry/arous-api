const ApiResponse = require('../utils/apiResponse');
const countryModel = require('../models/country');
const cityModel = require('../models/city');

class Places {
    async getCountriesAndCities(params) {
        let query = {};
        if (Object.keys(params).length > 0) {
            if (params.countryId && (params.countryId).length > 0) {
                try {
                    query = {
                        countryId: JSON.parse(params.countryId)
                    };
                    if (!query.countryId.length > 0) throw 'error';
                } catch (error) {
                    return ApiResponse([`wrong parameter value {${params.countryId}}`], null);
                }
            } else {
                return ApiResponse([`wrong {${Object.keys(params)}} Key`], null);
            }
        }

        let citiesWithCountries;
        try {
            citiesWithCountries = await cityModel.find(query, 'name').populate('countryId', 'name');
        } catch (error) {
            return ApiResponse(null, []);
        }


        // get all countries
        //  Remove Duplication country Objects in array
        let countriesObject = [...new Set(citiesWithCountries.map(country => {
            return JSON.stringify({
                _id: country.countryId._id,
                name: country.countryId.name
            });
        }))].map(obj => JSON.parse(obj));

        let countries = [];
        let result = countriesObject.map(country => {
            //get cities for same country
            countries = citiesWithCountries.filter((city) => {
                return city.countryId._id == country._id
            })
            let citiesrArr = countries.map(city => {
                return {
                    name: city.name,
                    _id: city._id
                }
            })

            return {
                name: country.name,
                _id: country._id,
                cities: citiesrArr
            }
        })

        return ApiResponse(null, result);
    }
}
module.exports = new Places();