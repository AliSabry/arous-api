const giftModel = require('../models/gift');
const dbErrorsHandler = require('../dataBase/dbErrorsHandler');
const ApiResponse = require('../utils/apiResponse');

class Gift {
    async getGifts(userId) {
        return giftModel.find({}).then(gifts => {
            if (gifts) {
                return ApiResponse(null, gifts.map(gift => {
                    return {
                        title: gift.title,
                        img: gift.img,
                        url: gift.url
                    }
                }));
            }
            throw 'err'
        }).catch(err => {
            return ApiResponse(null, []);
        })
    }
}

module.exports = new Gift();