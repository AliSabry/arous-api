'use strict'
const categoryModel = require('../models/category');
const sliderModel = require('../models/slider');
const userController = require('./user.controller');
const dbErrorsHandler = require('../dataBase/dbErrorsHandler');
const ApiResponse = require('../utils/apiResponse');

class Home {

    async getHomePage(userId) {
        let categories = await this.getCategories();
        let sliders = await this.getSlider();
        let cities = await userController.getUserCities(userId);

        if (sliders.err || categories.err || cities.errorMsg.lenght > 0) {
            let err = sliders.err || categories.err || cities.errorMsg;
            return ApiResponse(err, null);
        }
        return ApiResponse(null, { categories, sliders, cities: cities.data });
    }

    async getCategories() {
        return categoryModel.find({}, ['title', 'img']).then(sections => {
            return sections;
        }).catch(err => {
            return { err: dbErrorsHandler(err) };
        });
    }

    async getSlider() {
        return sliderModel.find({}, ['title', 'img']).then(sliders => {
            return sliders;
        }).catch(err => {
            return { err: dbErrorsHandler(err) };
        });
    }
}
module.exports = new Home();