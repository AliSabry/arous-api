const notificationModel = require('../models/notification');
const dbErrorsHandler = require('../dataBase/dbErrorsHandler');
const ApiResponse = require('../utils/apiResponse');

class Notification {
    async getNotifications(userId) {
        return notificationModel.find({}).then(notifies => {
            if (notifies) {
                return ApiResponse(null, notifies.map(notify => {
                    return {
                        title: notify.title,
                        img: notify.img,
                    }
                }));
            }
            throw 'err'
        }).catch(err => {
            return ApiResponse(null, []);
        })
    }
}

module.exports = new Notification();