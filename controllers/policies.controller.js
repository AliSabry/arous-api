"use strict";
const ApiResponse = require('../utils/apiResponse');
const policyModel = require('../models/policy');
const errorsHandler = require('../dataBase/dbErrorsHandler');

class Policy {
    async getSitePolicies() {

        return policyModel.findOne({}).then(polices => {
            if (polices) {
                return ApiResponse(null, {
                    polices: polices.description
                });
            }
            return ApiResponse(null, []);
        }).catch(err => {
            return ApiResponse(errorsHandler(err), null);
        });
    }
}
module.exports = new Policy();