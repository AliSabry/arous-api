'use strict'

const productModel = require('../models/product');
const userModel = require('../models/user');
const uploadImage = require('../utils/uploadImage');
const ApiResponse = require('../utils/apiResponse');
const dbErrHandler = require('../dataBase/dbErrorsHandler');
const Time = require('../utils/time');
const fs = require('fs');
const imagePath = process.cwd() + "\\public\\assets\\images\\products";
class Product {

    async addProduct(userId, product) {
        // empty body
        if (!Object.keys(product).length > 0) {
            return ApiResponse({
                msg: `product is requierd`
            }, null);
        }
        let allowedBody = ['img', 'title', 'description', 'priceFrom', 'priceTo', 'cityId', 'catogeryId', 'expiredAt', 'countryId'];
        // check empty values
        for (let i = 0; i < Object.keys(product).length; i++) {
            if (!allowedBody.includes(Object.keys(product)[i])) {
                return ApiResponse({
                    msg: `not valid {${Object.keys(product)[i]}} Key`
                }, null);
            }
            const element = product[`${Object.keys(product)[i]}`];
            if (!element) {
                return ApiResponse({
                    msg: `not valid {${Object.keys(product)[i]}} value`
                }, null);
            }
        }
        // save Product images array
        if (!product.img) {
            return ApiResponse({
                msg: `product images is requierd`
            }, null);
        }
        let savedImages = [];
        let images;
        try {
            images = JSON.parse(product.img);
            if (images.length > 0) {
                for (let i = 0; i < images.length; i++) {
                    savedImages.push(await uploadImage(images[i], 'products'));
                }
            }
        } catch (err) {
            return ApiResponse({
                msg: `not valid {img} value`
            }, null);
        }
        //convert date to time stamp
        if (product.expiredAt) {
            product.expiredAt = new Date(`${product.expiredAt}`).getTime();
        }
        const newProduct = {
            title: product.title,
            gallery: savedImages,
            description: product.description,
            priceFrom: product.priceFrom,
            priceTo: product.priceTo,
            userId: userId,
            cityId: product.cityId,
            countryId: product.countryId,
            catogeryId: product.catogeryId,
            createdAt: new Date().getTime(),
            expiredAt: product.expiredAt
        }

        return productModel.create(newProduct).then(product => {
            return ApiResponse(null, {
                msg: 'Product created successfully'
            });
        }).catch(err => {
            // Delete all images in err
            savedImages.forEach(img => {
                if (fs.existsSync(imagePath + `\\${img}`)) {
                    fs.unlinkSync(imagePath + `\\${img}`);
                }
            });
            return ApiResponse(dbErrHandler(err), null);
        })

    }

    async getTraderProducts(userId) {
        return await this.getProducts({
            userId: userId
        });
    }

    async getCustomerProducts(query) {

        if (!Object.keys(query).length) {
            return ApiResponse([{
                msg: 'cityId & categoryId are required'
            }], null);
        }
        if (!query.cityId || !query.catogeryId) {
            return ApiResponse([{
                msg: `cityId & categoryId are required`
            }], null);
        }
        return await this.getProducts({
            cityId: query.cityId,
            catogeryId: query.catogeryId
        })
    }

    async getProducts(query) {
        return productModel.find(query).then(products => {
            let productsObject = products.map(product => {
                let totalDays = Time.timePeriod(product.createdAt, product.expiredAt);
                let remainingDays = Time.timePeriod(new Date().getTime(), product.expiredAt);
                return {
                    _id: product._id,
                    title: product.title,
                    gallery: product.gallery,
                    rate: product.rate,
                    priceTo: product.priceTo,
                    priceFrom: product.priceFrom,
                    specialOffer: product.specialOffer,
                    duration: {
                        totalDays: totalDays,
                        remainingDays: remainingDays
                    }
                }
            })

            return ApiResponse(null, productsObject);
        }).catch(err => {
            return ApiResponse(null, []);
        })
    }

    async getProductDetails(query) {
        if (!query.productId) {
            return ApiResponse([{
                msg: 'productId is required'
            }], null);
        }
        return productModel.findById({
                _id: query.productId
            }, ['-__v', '-cityId', '-catogeryId', '-createdAt', '-expiredAt'])
            .populate('userId', ['name', 'Location', 'mobile', 'img', 'whatsApp']).then(product => {
                if (product) {
                    let trader = product.userId
                    const result = {
                        ...product._doc,
                        trader
                    };
                    delete(result.userId);
                    return ApiResponse(null, result);
                }
                throw 'err';
            }).catch(err => {
                return ApiResponse(null, []);
            });
    }

    async editProduct(product) {
        if (!product.productId) {
            return ApiResponse([{
                msg: 'productId is required'
            }], null);
        }
        let body = ['title', 'description', 'priceTo', 'catogeryId', 'priceFrom', 'productId', 'gallery'];
        // check empty values and invalid
        for (let i = 0; i < Object.keys(product).length; i++) {
            if (!body.includes(Object.keys(product)[i])) {
                return ApiResponse({
                    msg: `not valid {${Object.keys(product)[i]}} key`
                }, null);
            }
            const element = product[`${Object.keys(product)[i]}`];
            if (!element) {
                return ApiResponse({
                    msg: `not valid {${Object.keys(product)[i]}} value`
                }, null);
            }
        }

        if (product.gallery) {
            try {
                let newGallery = JSON.parse(product.gallery);
                if (!Array.isArray(newGallery)) {
                    return ApiResponse({
                        msg: `not valid {${newGallery}} value`
                    }, null);
                }
                // get saved images and delete if not selected in update
                let oldGallery = await productModel.findById({
                    _id: product.productId
                }).select('gallery');

                let oldImages = [];
                let newImages = [];
                // delete Old images
                for (let i = 0; i < oldGallery.gallery.length; i++) {
                    if (!newGallery.includes(oldGallery.gallery[i])) {
                        if (fs.existsSync(imagePath + `\\${oldGallery.gallery[i]}`)) {
                            fs.unlinkSync(imagePath + `\\${oldGallery.gallery[i]}`);
                        }
                    } else {
                        oldImages.push(oldGallery.gallery[i]);
                    }
                }
                // upload new Images 
                for (let k = 0; k < newGallery.length; k++) {
                    if (!oldImages.includes(newGallery[k])) {
                        let newImage = await uploadImage(newGallery[k], 'products');
                        newImages.push(newImage);
                    } else {
                        newImages.push(newGallery[k])
                    }

                }
                product.gallery = newImages;
            } catch (error) {
                return ApiResponse({
                    msg: `not valid {${product.gallery}} value`
                }, null);
            }

        }

        // product object updates
        let updatedProduct = {};
        Object.keys(product).forEach(key => {
            if (key != "productId") {
                updatedProduct[`${key}`] = product[`${key}`]
            }
        });

        return productModel.findByIdAndUpdate({
            _id: product.productId
        }, updatedProduct, {
            runValidators: true,
            new: true
        }).then(product => {
            if (product) {
                return ApiResponse(null, {
                    "msg": "product successfully updated"

                });
            }
            return ApiResponse(null, []);
        }).catch(err => {
            return ApiResponse(dbErrHandler(err), null);
        });
    }

    async deleteProduct(userId, query) {
        if (!query.productId) {
            return ApiResponse([{
                msg: 'productId is required'
            }], null);
        }
        return productModel.findOneAndDelete({
            $and: [{
                _id: query.productId
            }, {
                userId: userId
            }]
        }).then(del => {

            return ApiResponse(null, {
                "msg": "product successfully deleted"

            });

        }).catch(err => {
            return ApiResponse(dbErrHandler(err), null);
        })
    }

    async getSpecialOffers(userId) {
        let user = await userModel.findById({
            _id: userId
        }, ['role', 'countryId']);

        let products;
        switch (user.role) {
            case 'customer':
                products = await this.getProducts({
                    countryId: user.countryId,
                    specialOffer: true
                })
                break;
            case 'trader':
                products = await this.getProducts({
                    userId: userId,
                    specialOffer: true
                });
                break;
        }

        return products
    }

    async searchProduct(query) {
        if (!query.search) {
            return ApiResponse([{
                msg: 'search word is required'
            }], null);
        }
        let searchArr = [];
        // partial text search with regex
        let regex = new RegExp(`${query.search}`);
        let searchWord = {
            title: {
                $regex: regex,
                $options: 'i'
            }
        };
        searchArr.push(searchWord);
        // get other search keys if exist
        for (let i = 0; i < Object.keys(query).length; i++) {
            const element = query[`${Object.keys(query)[i]}`];
            if (element && Object.keys(query)[i] != 'search') {
                // range of price From - To
                if (Object.keys(query)[i] === 'priceFrom') {
                    searchArr.push({
                        [Object.keys(query)[i]]: {
                            $gte: element
                        }
                    });
                } else if (Object.keys(query)[i] === 'priceTo') {
                    searchArr.push({
                        [Object.keys(query)[i]]: {
                            $lte: element
                        }
                    });
                } else {
                    searchArr.push({
                        [Object.keys(query)[i]]: element
                    });
                }

            }
        }

        return await this.getProducts({
            $and: searchArr
        });
    }

}
module.exports = new Product();