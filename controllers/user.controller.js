"use strict";
const ApiResponse = require('../utils/apiResponse');
const UserModel = require('../models/user');
const errorsHandler = require('../dataBase/dbErrorsHandler');
const cityModel = require('../models/city');
const sessionModel = require('../models/session');
const citiesController = require('../controllers/countries_cities.controller');
const bcrypt = require('bcrypt');
const fs = require('fs');
const path = require('path');
const constants = require('../utils/constants');
const imagePath = process.cwd() + "\\public\\assets\\images\\users";
const uploadImage = require('../utils/uploadImage');

class User {
    //check user data and register
    async getProfile(userId) {
        return UserModel.findOne({
            _id: userId
        }).then(async (user) => {
            const userPlace = await cityModel
                .findOne({
                    _id: user.cityId
                }).populate('countryId');
            let profile = {
                name: user.name,
                email: user.email,
                mobile: user.mobile,
                address: user.address,
                img: user.img,
                city: userPlace.name,
                country: userPlace.countryId.name,
                role: user.role,
                location: {
                    lon: user.Location.lon,
                    lat: user.Location.lat
                },
                whatsApp: user.whatsApp,
                fb: user.fb,
                instagram: user.instagram,
                snapChat: user.snapChat
            }

            return ApiResponse(null, [profile]);
        }).catch((err) => {
            return ApiResponse(errorsHandler(err), null);
        });
    }

    //update user data profile
    async updateProfile(userId, userUpdate) {
        // check empty values
        for (let i = 0; i < Object.keys(userUpdate).length; i++) {
            const element = userUpdate[`${Object.keys(userUpdate)[i]}`];
            if (!element) {
                return ApiResponse([`not valid {${Object.keys(userUpdate)[i]}} value`], null);
            }
        }
        // save new user img in file
        let newImage;
        if (userUpdate.img) {
            try {
                newImage = await uploadImage(userUpdate.img, 'users');
                if (fs.existsSync(imagePath + `\\${newImage}`)) {
                    //get old user img and delete before update
                    UserModel.findById({
                        _id: userId
                    }).then(user => {
                        let oldImg = user.img;
                        if (fs.existsSync(imagePath + `\\${oldImg}`)) {
                            fs.unlinkSync(imagePath + `\\${oldImg}`);
                        }
                    }).catch(err => {
                        throw 'error'
                    });
                }

            } catch (error) {

                return ApiResponse([`img not saved`], null);
            }
        }
        userUpdate.img = newImage;

        return UserModel.findByIdAndUpdate({
                _id: userId
            }, userUpdate, {
                runValidators: true,
                new: true
            })
            .then((user) => {
                return ApiResponse(null, userUpdate);
            }).catch((err) => {
                return ApiResponse(errorsHandler(err), null);
            });
    }

    // edit password
    async updatePassword(userId, password) {
        if (password.newPassword && password.oldPassword) {
            var hashedPassword = password.newPassword;
            var user = await UserModel.findById({
                _id: userId
            });
            //check user password 
            const match = bcrypt.compareSync(password.oldPassword, user.password);
            if (match) {
                if (password.newPassword.length >= constants.Password.minLenght) {
                    hashedPassword = bcrypt.hashSync(password.newPassword, constants.Password.bcryptFactor);
                }
                return UserModel.findByIdAndUpdate({
                    _id: userId
                }, {
                    password: hashedPassword
                }, {
                    runValidators: true,
                    new: true
                }).then((data) => {
                    return ApiResponse(null, ['Password successfully changed']);
                }).catch((err) => {
                    return ApiResponse(errorsHandler(err), null);
                });
            } else {
                return ApiResponse(['password incorrect'], null);
            }

        } else {
            return ApiResponse('password required', null);
        }
    }

    // get user cities in country
    async getUserCities(userId) {
        const countryId = await UserModel.findById({
            _id: userId
        }, 'countryId').populate('countryId');
        return await citiesController
            .getCountriesAndCities({
                countryId: JSON.stringify(`${countryId.countryId._id}`)
            });
    }

    //add user location
    async addLocation(userId, body) {
        let keys = ['lat', 'lon'];
        for (let i = 0; i < Object.keys(body).length; i++) {
            //check invalid key
            if (!(keys.includes(Object.keys(body)[i]))) {
                return ApiResponse([{
                    msg: `not valid {${Object.keys(body)[i]}} key`
                }], null);
            }
            // check empty values
            if (!body[Object.keys(body)[i]]) {
                return ApiResponse([{
                    msg: `not valid {${Object.keys(body)[i]}} value`
                }], null);
            }
        }
        // check required key
        let deff = keys.filter(key => !Object.keys(body).includes(key));
        if (deff.length > 0) {
            return ApiResponse([{
                msg: ` {${deff}} is required`
            }], null);
        }

        let updates = {
            Location: {
                lon: body.lon,
                lat: body.lat
            }
        }
        // update user with location
        return UserModel.findOneAndUpdate({
            _id: userId
        }, updates, {
            runValidators: true,
            new: true
        }).then((user) => {
            return ApiResponse(null, {
                msg: "location successfully updated"
            });
        }).catch(err => {
            return ApiResponse(errorsHandler(err), null);
        })
    }

    // logout by delete user session
    async logoutUser(userId) {
        return sessionModel.deleteOne({
            userId: userId
        }).then(del => {
            return ApiResponse(null, {
                msg: 'logged out'
            });

        }).catch(err => {
            return ApiResponse(errorsHandler(err), null)
        })
    }

}

module.exports = new User();