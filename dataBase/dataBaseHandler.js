const errorsHandler = require('./dbErrorsHandler');
const ApiResponse = require('../utils/apiResponse');

//Operations ( Create , Delete , Update , Read )

class DataBaseOperations {

    // ADD
    async saveObject(newObject) {
        return newObject.save().then((data) => {
            return ApiResponse(null, [data]);
        }).catch((err) => {
            return ApiResponse(errorsHandler(err), null);
        });
    }

    // GET One
    getObject(modelObject, query) {
        modelObject.findOne(query).then((data) => {
            return ApiResponse(null, [data]);
        }).catch((err) => {
            return ApiResponse(errorsHandler(err), null);
        });
    }

    // GET All
    getObjectsArray(modelObject, query) {
        modelObject.find(query).then((data) => {
            return ApiResponse(null, [data]);
        }).catch((err) => {
            return ApiResponse(errorsHandler(err), null);
        });
    }

    // Update
    updateObject(modelObject, query) {
        return modelObject.update(query, dataObject, { new: true }, callBack(err, data));
    }

    // Delet
    deleteObject(modelObject, query) {
        modelObject.deleteOne(query).then((data) => {
            return ApiResponse(null, [data]);
        }).catch((err) => {
            return ApiResponse(errorsHandler(err), null);
        });
    }
}

module.exports = new DataBaseOperations();