const config = require("./../config/env")
const mongoose = require('mongoose');

mongoose.connect(config.DB.URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("DB Connected!");
    }).catch((err) => {
        console.log(err);
    })