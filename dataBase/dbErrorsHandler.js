module.exports = (err) => {
    let errorsMsg = [];
    if (err.name === 'MongoError' && err.code === 11000) {
        let key = (JSON.stringify(err.keyValue).toString().split(':')[0]).replace(/"/g, '').split('{')[1];
        let value = JSON.stringify(err.keyValue).toString().replace(/"/g, '');
        errorsMsg.push({
            [key]: `${value} already exists`
        });
    }
    if (!err.errors) {
        if (err.name === 'CastError') {
            let result = `${err.path} is a ${err.kind}`;
            errorsMsg.push({
                [err.path]: result
            });

        }
    }
    for (let field in err.errors) {
        if ((err.errors[field].message).toString().includes('Path')) {
            let msg = (err.errors[field].message).toString().split('Path');
            var result;
            if (msg[1] != null) {
                result = msg[1].toString().replace(/`/g, '');
            }
        } else if (err.errors[field].name === 'CastError') {
            result = `${err.errors[field].path} is a ${err.errors[field].kind}`
        } else {
            result = (err.errors[field].message).toString().replace(/"/g, '');
        }
        errorsMsg.push({
            [field]: result
        });
    }
    return errorsMsg;
}