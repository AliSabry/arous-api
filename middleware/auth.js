"use strict";
const Session = require('../models/session');
const ApiResponse = require('../utils/apiResponse');
const Constants = require('../utils/constants');

module.exports = ((req, res, next) => {
    let errors = [];
    let userToken = req.headers.token;
    if (userToken) {
        return Session.findOne({
            token: userToken
        }).then(async (session) => {
            if (userToken === session.token) {
                let sessionTime = (new Date().getTime()) - (session.createdAt);
                if (sessionTime >= Constants.Session_Expiered_Time) {
                    await Session.deleteOne({
                        token: userToken
                    });
                    errors.push({
                        msg: 'Unauthorized'
                    });
                    return res.status(302).json(ApiResponse(errors, null));
                } else {
                    req.userId = session.userId;
                    return next();
                }
            } else {
                throw 'err';
            }
        }).catch((err) => {
            errors.push({
                msg: 'token not valid'
            });
            return res.status(404).json(ApiResponse(errors, null));
        });
    } else {
        errors.push({
            msg: 'token is required'
        });
        return res.status(401).json(ApiResponse(errors, null));
    }
});