const UserModel = require('../models/user');
const ApiResponse = require('../utils/apiResponse');
class Roles {
    //Check user roles
    async isCustomer(req, res, next) {
        let customer = await checkRole(req.userId, 'customer');
        if (customer) {
            return next();
        } else {
            return res.status(302).json(ApiResponse({
                msg: 'Unauthorized '
            }, null));
        }
    }

    async isTrader(req, res, next) {
        let trader = await checkRole(req.userId, 'trader');
        if (trader) {
            return next();
        } else {
            return res.status(302).json(ApiResponse({
                msg: 'Unauthorized '
            }, null));
        }
    }
}

function checkRole(userId, role) {
    return UserModel.findOne({
        _id: userId
    }).and({
        role: role
    }).then(user => {
        if (user) {
            return true;
        }
        return false;

    }).catch(err => {
        return false
    });
}
module.exports = new Roles();