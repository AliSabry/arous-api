const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    img: {
        type: String,
        trim: true,
        lowercase: true,
        required: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    }

})
module.exports = mongoose.model("Category", categorySchema);