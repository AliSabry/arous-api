const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const citySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },

    countryId: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
        required: true
    }

})
module.exports = mongoose.model("City", citySchema);