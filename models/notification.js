const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const notifySchema = new Schema({
    img: {
        type: String,
        trim: true,
        lowercase: true,
        required: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }

})
module.exports = mongoose.model("Nontify", notifySchema);