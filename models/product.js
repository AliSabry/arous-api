const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    gallery: [{
        type: String,
        lowercase: true,
        trim: true,
        required: true

    }],
    rate: {
        type: Number,
    },
    description: {
        type: String,
        trim: true,
        required: true
    },
    priceFrom: {
        type: Number,
        required: true,
        min: 0,
    },
    priceTo: {
        type: Number,
        required: true
    },
    specialOffer: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Number,
        required: true
    },
    expiredAt: {
        type: Number,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    cityId: {
        type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
    },
    countryId: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
        required: true
    },

    catogeryId: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }

})
productSchema.index(({
    title :'text'
}))
module.exports = mongoose.model("Product", productSchema);