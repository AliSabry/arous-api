const randtoken = require('rand-token').suid;
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sessionSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    token: {
        type: String,
        default: randtoken(16),
        unique: true
    },
    createdAt: {
        type: Number,
        required: true
    }

})
module.exports = mongoose.model("Session", sessionSchema);