const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sliderSchema = new Schema({
    img: {
        type: String,
        trim: true,
        lowercase: true,
        required: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    }

})
module.exports = mongoose.model("Slider", sliderSchema);