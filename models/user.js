const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: ['user name is required']
    },
    email: {
        type: String,
        lowercase: true,
        trim: true,
        unique: [true, 'email already exist'],
        required: true,
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Please fill a valid email address']
    },
    password: {
        type: String,
        trim: true,
        required: ['Password is required'],
        minlength: 8
    },
    Location: {
        lon: {
            type: String,
        },
        lat: {
            type: String,
        }
    },
    weddingDate: {
        type: Number,
        required: ['Wedding Date is required']
    },
    mobile: {
        type: Number,
        trim: true,
        unique: true,
        required: ['mobile number is required'],
    },
    img: {
        type: String,
        trim: true
    },
    whatsApp: {
        type: Number,
        trim: true,
    },
    fb: {
        type: String,
        lowercase: true,
        trim: true,
    },
    instagram: {
        type: String,
        lowercase: true,
        trim: true,
    },
    snapChat: {
        type: String,
        lowercase: true,
        trim: true,
    },
    address: {
        type: String
    },
    role: {
        type: String,
        required: true,
        lowercase: true,
        trim: true,
    },
    cityId: {
        type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
    },
    countryId: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
        required: true
    }

})
module.exports = mongoose.model("User", userSchema);