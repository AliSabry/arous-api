'use strict'

const auth = require('../controllers/authentication.controller');
const apiSend = require('../utils/apiSend');
const prefix = 'user';

module.exports = (app) => {
    // Register new User
    let result;
    app.post(`/${prefix}/register`, async(req, res, next) => {
        //handle body data and send response
        result = await auth.create(req.body);
        apiSend(res, result);
    });

    // User Login
    app.post(`/${prefix}/login`, async(req, res, next) => {
        //handle body data and send response
        result = await auth.login(req.body);
        apiSend(res, result);
    });
}