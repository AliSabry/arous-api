'use strict'
const controller = require('../controllers/countries_cities.controller');
const apiSend = require('../utils/apiSend');
module.exports = (app) => {

    app.get('/places/countries_cities', async(req, res, next) => {
        apiSend(res, await controller.getCountriesAndCities(req.query));
    })
}