'use strict'

const giftController = require("../controllers/gifts.controller");
const apiSend = require('../utils/apiSend');
const prefix = 'gifts';

module.exports = (app) => {
    // User Profile
    app.get(`/${prefix}/giftList`, async (req, res, next) => {
        //handle body data and send response
        let result = await giftController.getGifts(req.userId);
        apiSend(res, result);
    });
}