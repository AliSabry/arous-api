'use strict'

const homeController = require("../controllers/home.controller");
const apiSend = require('../utils/apiSend');
const ApiResponse = require('../utils/apiResponse');
const prefix = 'main';

module.exports = (app) => {
    // Home Page category & slider & cities
    app.get(`/${prefix}/home`, async(req, res, next) => {
        //handle body data and send response
        let result = await homeController.getHomePage(req.userId);
        apiSend(res, result);
    });
    // get all categories
    app.get(`/${prefix}/categories`, async(req, res, next) => {
        //handle body data and send response
        let categories = await homeController.getCategories();
        if (categories.err) {
            apiSend(res, ApiResponse(categories.err, null));
        } else {
            apiSend(res, ApiResponse(null, categories));
        }
    });
    // get all sliders
    app.get(`/${prefix}/sliders`, async(req, res, next) => {
        //handle body data and send response
        let sliders = await homeController.getSlider();
        if (sliders.err) {
            apiSend(res, ApiResponse(sliders.err, null));
        } else {
            apiSend(res, ApiResponse(null, sliders));
        }
    });


}