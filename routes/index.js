"use strict"

module.exports = (app) => {
    // user register and login routes
    require("./authentication.route")(app);
    require('./countries_cities.route')(app);

    //middleware auth for API
    app.use((req, res, next) => {
        require("../middleware/auth")(req, res, next);
    });
    // user profile read & update & delete
    require("./user.route")(app);
    // Customer Home get slider & category
    require('./home.route')(app);
    // Productes
    require('./products.route')(app);
    // polices
    require('./policy.route')(app);
    // user gifts
    require('./gifts.route')(app);
    // user notificationes
    require('./notification.route')(app);
}