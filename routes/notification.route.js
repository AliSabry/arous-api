'use strict'

const notifyController = require("../controllers/notification.controller");
const apiSend = require('../utils/apiSend');
const prefix = 'notification';

module.exports = (app) => {
    // User Profile
    app.get(`/${prefix}/getNotify`, async (req, res, next) => {
        //handle body data and send response
        let result = await notifyController.getNotifications(req.userId);
        apiSend(res, result);
    });
}