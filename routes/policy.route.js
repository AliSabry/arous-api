const policiesController = require("../controllers/policies.controller");
const apiSend = require('../utils/apiSend');
const prefix = 'site';

module.exports = (app) => {
    // User Profile
    app.get(`/${prefix}/policy`, async (req, res, next) => {
        //handle body data and send response
        let result = await policiesController.getSitePolicies();
        apiSend(res, result);
    });
}