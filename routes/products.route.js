'use strict'

const productController = require("../controllers/products.controller");
const roles = require('../middleware/user.roles');
const apiSend = require('../utils/apiSend');
const prefix = 'product';

module.exports = (app) => {
    //trader add new product
    app.post(`/${prefix}/add_product`, roles.isTrader, async (req, res, next) => {
        //handle body data and send response
        let result = await productController.addProduct(req.userId, req.body);
        apiSend(res, result);
    });

    //get trader products list
    app.get(`/${prefix}/trader_products`, roles.isTrader, async (req, res, next) => {
        let result = await productController.getTraderProducts(req.userId);
        apiSend(res, result);
    });

    //get custoemr products list by city & category
    app.get(`/${prefix}/customer_products`, roles.isCustomer, async (req, res, next) => {
        let result = await productController.getCustomerProducts(req.query);
        apiSend(res, result);
    });

    //get product details by productID
    app.get(`/${prefix}/details`, async (req, res, next) => {
        let result = await productController.getProductDetails(req.query);
        apiSend(res, result);
    });

    //Trader edit product by ID
    app.patch(`/${prefix}/edit`, roles.isTrader, async (req, res, next) => {
        let result = await productController.editProduct(req.body);
        apiSend(res, result);
    });

    //Trader delete product
    app.delete(`/${prefix}/delete`, roles.isTrader, async(req, res, next) => {
        let result = await productController.deleteProduct(req.userId,req.query);
        apiSend(res, result);
    });

    // Get special offers product list 
    app.get(`/${prefix}/special_offers`, async (req, res, next) => {
        let result = await productController.getSpecialOffers(req.userId);
        apiSend(res, result);
    });

    // Search in product
    app.get(`/${prefix}/search`, async (req, res, next) => {
        let result = await productController.searchProduct(req.query);
        apiSend(res, result);
    });
}