'use strict'

const user = require("../controllers/user.controller");
const apiSend = require('../utils/apiSend');
const prefix = 'user';

module.exports = (app) => {
    // User Profile
    app.get(`/${prefix}/profile`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.getProfile(req.userId);
        apiSend(res, result);
    });
    // edit User Profile
    app.patch(`/${prefix}/edit_profile`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.updateProfile(req.userId, req.body);
        apiSend(res, result);
    });
    // edit User Password
    app.patch(`/${prefix}/edit_password`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.updatePassword(req.userId, req.body);
        apiSend(res, result);
    });
    //get user cities
    app.get(`/${prefix}/get_cities`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.getUserCities(req.userId);
        apiSend(res, result);
    });
    // User logout delete Session
    app.post(`/${prefix}/logout`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.logoutUser(req.userId);
        apiSend(res, result);
    });
    // User Add location
    app.post(`/${prefix}/add_location`, async (req, res, next) => {
        //handle body data and send response
        let result = await user.addLocation(req.userId, req.body);
        apiSend(res, result);
    });

}