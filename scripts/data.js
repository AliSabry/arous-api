require("../dataBase/dbConnection");

const countryModel = require('../models/country');
const cityModel = require('../models/city');
const categorieModel = require('../models/category');
const giftModel = require('../models/gift');
const notifyModel = require('../models/notification');

let countries = [{
    name: 'Egypt'
}, {
    name: 'UAE'
}, {
    name: 'Saudi Arabia'
}];

let cities = [{
    //Egypt
    countryId: "5dbad9c9cf963d289096582d",
    name: 'Mansoura'
}, {
    countryId: "5dbad9c9cf963d289096582d",
    name: 'Cairo'
}, {
    //UAE
    countryId: "5dbad9c9cf963d289096582e",
    name: 'Dubai'
}, {
    countryId: "5dbad9c9cf963d289096582e",
    name: 'Sharja'
}, {
    //SA
    countryId: "5dbad9c9cf963d289096582f",
    name: 'Dammam'
}, {
    countryId: "5dbad9c9cf963d289096582f",
    name: 'Riyadh'

}]

let categories = [{
        img: '151510.png',
        title: 'Abaya'

    },
    {
        img: '15134613.png',
        title: 'Beauty'
    },
    {
        img: '123456.png',
        title: 'Dresses'
    },
    {
        img: '12154153.png',
        title: 'Hotels'
    }
]

let notify = [{

        img: '496263.png',
        title: 'notify 2',
        userId: '5dbd4f5e807ffc294050d204'
    },
    {

        img: '5616161.png',
        title: 'notify 3',
        userId: '5dbd4f5e807ffc294050d204'
    },
    {

        img: '1548562.png',
        title: 'notify 4',
        userId: '5dbd4f5e807ffc294050d204'
    }
]

let gifts = [{

        img: '496263.png',
        title: 'asmple 2',
        url: 'www.fb.com',
        userId: '5dbd4f5e807ffc294050d204'
    },
    {

        img: '5616161.png',
        title: 'asmple 3',
        url: 'www.fb.com',
        userId: '5dbd4f5e807ffc294050d204'
    },
    {

        img: '1548562.png',
        title: 'asmple 4',
        url: 'www.fb.com',
        userId: '5dbd4f5e807ffc294050d204'
    }
]
save(notifyModel, notify);

async function save(model, data) {
    model.create(data).then(result => {
        console.log(result);

    }).catch(err => {
        console.log(err);
    });
}