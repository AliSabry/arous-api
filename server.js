'use strict' // any syntacs error check in run time

//DB Connection
require("./dataBase/dbConnection");

const env = require('./config/env');
const express = require('express');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || env.SERVER.PORT;
const app = express();

app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

// Routes index
require("./routes/index")(app);

//Start server
app.listen(PORT, () => console.log(`server runnig port ${PORT}`));