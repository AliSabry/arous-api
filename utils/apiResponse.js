module.exports = ((err, data) => {
    if (err === null) {
        return {
            success: true,
            data: data,
            errorMsg: []
        }
    } else {
        return {
            success: false,
            data: [],
            errorMsg: err
        }
    }
})