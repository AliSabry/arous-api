const Code = require('./constants');

module.exports = (res, result) => {
    if (result.success) {
        res.status(Code.StatusCode.OK).json(
            result
        );
    } else {
        res.status(Code.StatusCode.BAD_REQUEST).json(
            result
        );
    }
}