module.exports = {
    StatusCode: {
        OK: 200,
        CREATED: 201,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        NOT_FOUND: 404,
        SERVER_ERROR: 500
    },
    Password: {
        minLenght: 8,
        bcryptFactor: 12
    },
    Session_Expiered_Time: 24 * 60 * 60 * 1000
}