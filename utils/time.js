class Time {
    convertTime(timeStamp) {
        return new Date(timeStamp);
    }

    timePeriod(start, end) {

        let def = Math.abs(end - start);
        let days = Math.floor(def / (1000 * 24 * 60 * 60))

        return days;
    }
}
module.exports = new Time();