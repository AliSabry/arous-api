const fs = require('fs');
const path = require('path');
const imagePath = process.cwd() + "\\public\\assets\\images\\";

module.exports = (imgBase64, imagefile) => {
    /*TODO check image base64 and ext regix*/
    // to create some random id or name for image name
    const imgName = new Date().getTime().toString();
    // Remove header
    let stringImg = imgBase64.split(';base64,');
    let ext;
    let convertedImg;
    if (stringImg.length > 1) {
        ext = stringImg[0].split("/").pop();
        convertedImg = new Buffer.from(stringImg[1], 'base64');
    } else {
        ext = 'png'
        convertedImg = new Buffer.from(stringImg[0], 'base64');
    }

    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(imagePath, imagefile, imgName + `.${ext}`), convertedImg, (err) => {
            if (!err) {
                resolve(imgName + `.${ext}`);
            } else {
                reject(err);
            }
        });
    });
}